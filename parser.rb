require 'open-uri'
require 'nokogiri'
require 'pry'

DAYS = {
  monday: 2,
  tuesday: 3,
  wednesday: 4,
  thursday: 5,
  friday: 6,
  saturday: 7,
  sunday: 8
}.freeze

NAMES = {
  'Солнечная Сторона Невского Проспекта' => '“Солнечная Сторона Невского Проспекта“',
  'Сертолово' => '“Сертолово“'
}.freeze

REP = {
  '“Выход Есть“' => '“Выход Есть“ (ВВ88)',
  '“Любовь“' => '“Любовь“ (ВВ88)'
}.freeze

doc = Nokogiri::HTML(open('https://aapiter.ru/raspisanie_aa/'))
table = doc.css('.entry.themeform table tbody tr')

table_hash = { groups: [] }

def format_title(title, index)
  {
    id: (index - 1).to_s,
    row_type: 'title',
    content: {
      table_title: title
    }
  }
end

def schedule_parse(row)
  return [] if row.text.strip.empty?

  parse_day_time(row)
end

def trim_arr(arr)
  arr.map { |i| i.gsub(/[[:space:]]/, '') }
end

def parse_day_time(row)
  group = []
  opened = trim_arr(row.css("span[style='color: #ff0000;']").map(&:text))
  closed = trim_arr(row.text.split(' '))
  closed -= opened
  group += time_type(opened, 'open') unless opened.empty?
  group += time_type(closed, 'closed') unless closed.empty?
  group
end

def time_type(arr, type)
  arr.collect do |s|
    {
      type: type,
      time: s
    }
  end
end

def format_schedule_row(row, index)
  {
    id: (index - 1).to_s,
    row_type: 'schedule',
    content: {
      group_name: split_name_subway(row[0])[0],
      suway_station: split_name_subway(row[0])[1],
      group_addres: row[1].text.strip.gsub(/[[:space:]]/, ' ').tr("\n", ' '),
      days: days(row)
    }
  }
end

def split_name_subway(row)
  row = name_trim(row)
  NAMES.keys.any? do |word|
    row.sub!(word, NAMES[word]) if row.include?(word)
  end

  group_name = group_name(row)

  row.slice!("#{group_name} ")
  suway_station = row

  [group_name, suway_station]
end

def group_name(str)
  str = "“#{str[/“(.*?)“/m, 1]}“"
  REP[str] || str
end

def name_trim(row)
  rep = ['”', '«', '»', '‘']
  str = row.text.strip.tr("\n", ' ')
  rep.each { |i| str.gsub!(i, '“') }
  str
end

def days(row)
  days = {}
  DAYS.each { |day, num| days[day] = schedule_parse(row[num]) }
  days
end

table.each_with_index do |row, index|
  next if [0, 1].include?(index)

  table_hash[:groups] << if row.at_css('[colspan="9"]')
                           format_title(row.text.strip, index)
                         else
                           format_schedule_row(row.css('td'), index)
                         end
end

File.open('parsed.json', 'w') do |file|
  file.write("var data='")
  file.write(table_hash.to_json)
  file.write("'")
end
